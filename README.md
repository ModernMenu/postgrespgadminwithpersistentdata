# Installation de containers PostgreSQL et pgAdmin pour test (Avec persistance des DATA)

Cloner ce repository

Se rendre dans le dossier et lancer la commande:

```
docker-compose up -d
```

Dans un navigateur, aller sur http://localhost:5050

![Interface de connexion](images/pgadmin0.PNG)

On s'identifie avec les infos renseignées dans le docker-compose.yml:

identifiant: admin@admin.com
mot de passe: root

Quand on est connecté, on clique sur "ajouter un nouveau serveur"

Et on remplit les champs comme suit:

![Onglet Général](images/pgadmin1.PNG)

Et dans l'onglet connexion, on renseigne le nom du container dans le nom d'hôte (pg_container) et on définie les login/mdp pour se connecter à la BDD (root/root)

![Onglet Connexion](images/pgadmin2.PNG)

Il y a persistence de données, même si on arrête nos containers avec la commande

```
docker-compose down
```

Dès qu'on relancera le docker-compose, il retrouvera les données stockées dans les volumes (Données de PostgreSQL et PgAdmin)